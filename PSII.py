#! /usr/bin/env python
print """ Pythonic Story Interaction Interface (PSII), v0.1 """

import os, sys, pygame
from pygame.locals import *
from src.loadstuff import *
from src.level import Level
from src.menu import *

if not pygame.font:
    print 'Warning: no fonts detected; fonts disabled'
if not pygame.mixer:
    print 'Warning: no sound driver detected;  sound disabled'


class PSII(object):
    def __init__(self, level="map", parent=None):
        pygame.init()
        self._fps_clock = pygame.time.Clock()
        self._fps = 30
        self._parent = parent

        self._level = self._load_level(level)
        self._config = self._load_config()
        self._setup_ui()

        self._window_surface = pygame.display.set_mode(self._window_size,
                                                       pygame.DOUBLEBUF)
        self._drawing_board = pygame.Surface(self._dimensions)
        self._title = "PSII: " + self._level.title
        pygame.display.set_caption(self._title)
        self._level = self._load_level_graphics(level)

        self._mode = 'main'
        self.keyboard = {K_UP:'up', K_k:'up',
                         K_DOWN:'down', K_j:'down',
                         K_LEFT:'left', K_h:'left',
                         K_RIGHT:'right', K_l:'right',
                         K_a:'a', K_s:'b', K_d:'c',
                         K_RETURN:'start', K_SPACE:'start',
                         K_ESCAPE:'quit', K_F1:'help'}

    def _load_level(self, level):
        """ Load level data from file. """
        return Level(level)

    def _setup_ui(self):
        config = self._config

        dimensions = map(lambda x: int(x),
                         config['main_screen']['dimensions'])
        self._dimensions = dimensions

        scale = int(config['main_screen']['def_scale'])
        self._scale = scale

        self._window_size = (dimensions[0]*scale, dimensions[1]*scale)
        print "Using resolution: {0}, with a scale factor of {1}".\
              format(self._window_size, self._scale)

        # Setup some meta data that might be needed:
        bg_color = config['main_screen']['bg_color']
        self._bg_color = pygame.Color(bg_color) # HACK

        top_left   = map(lambda x: int(x),
                         config['main_screen']['portrait']['top_left'])
        dimensions = map(lambda x: int(x),
                         config['main_screen']['portrait']['dimensions'])
        self._portrait_area = (top_left, dimensions)

        top_left   = map(lambda x: int(x),
                         config['main_screen']['portrait']['top_left'])
        dimensions = map(lambda x: int(x),
                         config['main_screen']['portrait']['dimensions'])
        self._location_area = (top_left, dimensions)

        top_left   = map(lambda x: int(x),
                         config['main_screen']['portrait']['top_left'])
        dimensions = map(lambda x: int(x),
                         config['main_screen']['portrait']['dimensions'])
        self._info_box_area = (top_left, dimensions)

        top_left   = map(lambda x: int(x),
                         config['main_screen']['portrait']['top_left'])
        dimensions = map(lambda x: int(x),
                         config['main_screen']['portrait']['dimensions'])
        self._text_box_area = (top_left, dimensions)

        top_left   = map(lambda x: int(x),
                         config['main_screen']['portrait']['top_left'])
        dimensions = map(lambda x: int(x),
                         config['main_screen']['portrait']['dimensions'])
        self._main_menu_area = (top_left, dimensions)

        self._main_menu = Main_Menu(config, 'Main', self)
        self._move_menu = Generic_Menu(config, 'Move', self)
        self._look_menu = Generic_Menu(config, 'Look', self)
        self._take_menu = Generic_Menu(config, 'Take', self)
        self._use_menu  = Generic_Menu(config, 'Use',  self)
        self._drop_menu = Generic_Menu(config, 'Drop', self)

        self._setup_main_menu()

    def _setup_main_menu(self):
        items = {   'Move':'move', 'Look':'look',
                    'Take':'take', 'Use':'use',
                    'Drop':'drop'}
        self._main_menu.set_items(items)

    def _load_config(self):
        return load_config(os.path.join(self._level.level_path,
                           'ui_settings.cfg'))

    def _load_level_graphics(self, level):
        """ Load level graphics. """
        # Setup paths (should be done from self._level):
        level_path = self._level.level_path
        image_path = os.path.join(level_path, 'graphics')
        sound_path = os.path.join(level_path, 'sounds')
        bg_image = os.path.join(image_path, self._config['main_screen']\
                                                        ['bg_image'])

        # Load images:
        self._bg_surface, temp = load_image(bg_image)

    def close(self):
        """ Close game. """
        print "Quitting game..."
        pygame.quit()
        sys.exit() # This should call a method in parent!

    def _handle_inputs(self):
        """ Should later handle inputs from user. """
        for event in pygame.event.get():
            if event.type == QUIT:
                self.close()
            elif event.type == KEYDOWN:
                self._relay_input(event.key)

    def _relay_input(self, event_key):
        print "Current mode: " + self._mode
        if self._mode == 'main':
            if event_key in self.keyboard.keys():
                print "Key pressed: " + self.keyboard[event_key] + \
                      " (" + str(event_key) + ")"
                if self.keyboard[event_key] == 'quit':
                    self.close()
                print self._main_menu._interact(self.keyboard[event_key])
                print self._main_menu._index
            else:
                print "Unknown key pressed." + " (" + str(event_key) + ")"

    def main_loop(self):
        """ Main game loop. """
        while True:
            self._drawing_board.fill(self._bg_color)

            self._handle_inputs()

            if self._mode == 'main':
                self._draw_main_view()
            elif self._mode == 'move':
                self._draw_inventory_view()
            elif self._mode == 'inventory':
                self._draw_movement_view()
            elif self._mode == 'message':
                pass
            else:
                print "Warning: unknown game mode."

            pygame.transform.scale(self._drawing_board, self._window_size,
                                   self._window_surface)

            pygame.display.flip()
            self._fps_clock.tick(30)

    def _draw_main_view(self):
        """ Draw the main interaction window. """
        self._drawing_board.blit(self._bg_surface, (0, 0))

    def _draw_inventory_view(self, initialise=False):
        """ Draw the inventory dialog. """
        pass

    def _draw_movement_view(self, initialise=False):
        """ Draw the movement dialog. """
        pass

    def _help(self):
        pass

def main():
    gui = PSII()
    pygame.init()
    gui.main_loop()

if __name__ == "__main__":
    sys.exit(main())

