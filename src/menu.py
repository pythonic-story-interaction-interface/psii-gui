import os, pygame
from sprites import *
from loadstuff import *

class Menu(object):
    def __init__(self, menu_type, config, name, owner=None):
        super(Menu, self).__init__()
        self.menu_type = menu_type
        self._config = config[menu_type]
        self.name = name
        self._owner = owner
        self._bg_color = pygame.Color(self._config['bg_color'])
        top_left   = map(lambda x: int(x), self._config['top_left'])
        dimensions = map(lambda x: int(x), self._config['dimensions'])
        self._menu_area = (top_left, dimensions)
        self._rows = int(self._config['rows'])
        self._cols = int(self._config['cols'])
        self._size = self._rows*self._cols
        self._items = []
        for c in xrange(self._cols):
            for r in xrange(self._rows):
                self._items.append(None)
                
        self._index = 0

    def _load_images(self):
        config = self._config
        # Setup paths (should be done from self._level):
        level_path = self_owner._level.level_path
        image_path = os.path.join(level_path, 'graphics')

        active_image   = os.path.join(image_path, config['active_icon'])
        active_image_frames = config['active_icon_frames']
        active_image_fps    = config['active_icon_fps']
        inactive_image = os.path.join(image_path, config['inactive_icon'])
        inactive_image_frames = config['inactive_icon_frames']
        inactive_image_fps    = config['inactive_icon_fps']

        # Load sprites:
        #self._active_image, temp = load_image(active_image) 
        #self._inactive_image, temp = load_image(inactive_image) 

    def _interact(self, command):
        if command.lower() == "up":
            self._index += 1
        elif command.lower() == "down":
            self._index -= 1
        elif command.lower() == "left":
            self._index -= self._rows
        elif command.lower() == "right":
            self._index += self._rows
        elif command.lower() == "a":
            return self._select_item()
        elif command.lower() == "b":
            self._cancel()
        elif command.lower() == "c":
            return self._select_item()

        self._correct_cursor()
        
    def _correct_cursor(self):
        self._index %= self._rows*self._cols
        while self._index > 0 and self._items[self._index] is None:
            self._index -= 1

    def _cancel(self):
        pass

    def _select_item(self):
        if self._items[self._index] is not None:
            return self._items[self._index].get_action()

    def _add_item(self, item, position=-1):
        if position >= 0:
            try:
                self._items[position] = item
            except IndexError:
                print "Illegal index:", position
                print "Aborting addition..."
        else:
            index = 0
            while self._items[index] is not None and \
                  index <= self._cols*self._rows:
                index +=1 
            if self._items[index] is None:
                self._items[index] = item
            else:
                print "Menu is full!"

    def set_items(self, items):
        for item in items.keys():
            self._add_item(Menu_Item(item, items[item], self))
    
    def get_items(self):
        return self._items

    def _clear_item(self, position):
        self._items[position] = None

    def clear_items(self):
        for n in xrange(self._rows*self._cols):
            self._clear_item(n)
            

class Main_Menu(Menu):
    def __init__(self, config, name, owner):
        super(Main_Menu, self).__init__('main_menu', config, name, owner)

class Generic_Menu(Menu):
    def __init__(self, config, name, owner):
        super(Generic_Menu, self).__init__('generic_menu', config, name, owner)

    def _load_images(self):
        super(Main_Menu, self)._load_images()

        config = self._config
        # Setup paths (should be done from self._level):
        level_path = self_owner._level.level_path
        image_path = os.path.join(level_path, 'graphics')

        option_box_image = os.path.join(image_path, config['option_box_image'])
        self._option_box_image, temp = load_image(option_box_image)

        
class Movement_Menu(Menu):
    def __init__(self, config, name, owner):
        super(Movement_Menu, self).__init__('generic_menu', config, name, owner)
        self._index = 7

    def _load_images(self):
        pass


class Menu_Item(object):
    def __init__(self, name, action, owner):
        self.name = name
        self._action = action
        self._owner = owner

    def get_action(self):
        return self._action

    def get_name(self):
        return self.name

