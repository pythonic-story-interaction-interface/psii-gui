import os, sys, pygame
from configobj import ConfigObj

def load_image(image_path, colorkey=None):
    try:
        image = pygame.image.load(image_path)
    except pygame.error, message:
        print "Image not found:", image_path
        raise SystemExit, message

    image = image.convert()

    if colorkey is not None:
        if colorkey == -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey, RLEACCEL)

    return image, image.get_rect()


def slice_sprite(source_image, n_frames, colorkey=None):
    ''' 
    Slice sprite image into n_frames frames. Return list of frame images.
        
    The source_image can be any height, however the frame width must be the same 
    for all frames, and hence the source_image width must be:
        n_frames * desired frame width
    '''

    images = []
    source_width, source_height = source_image.get_size()
    w, h = source_width / frames, source_height
    for i in xrange(frames):
        images.append(master_image.subsurface( (i*w, 0, w, h) ))
        images[-1] = images[-1].convert()

        if colorkey is not None:
            if colorkey == -1:
                colorkey = images[-1].get_at((0, 0))
            images[-1].set_colorkey(colorkey, RLEACCEL)

    return images


def load_sound(soundpath):
    class NoneSound:
        def play(self): 
            pass
    
    if not pygame.mixer:
        return NoneSound()
    
    try:
        sound = pygame.mixer.Sound(sound_path)
    except pygame.error, message:
        print 'Sound not found:', sound_path
        raise SystemExit, message

    return sound

def load_config(filename):
    return ConfigObj(filename)

def load_font(filenamei, from_system=False):
    pass
