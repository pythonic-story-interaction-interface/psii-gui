import pygame
from loadstuff import *
from warnings import warn

class Animation(pygame.sprite.Sprite):
    """A class for keeping track of animation meta-data."""

    def __init__(self, owner, source, layer=None, start_state=None):
        pygame.sprite.Sprite.__init__(self)
        self.owner = owner
        self.source = source
        self.name = self.source.name
        self.layer = layer
        self.states = self.source['states'].keys()
        self.frames = {}
        self.n_frames = {}
        self.fps = {}

        for state in self.states:
            try:
                colorkey = self.source['states'][state]['colorkey']
                temp_image = load_image(self.source['states'][state]['image'], 
                                        colorkey)
                self.n_frames[state] = self.source['states'][state]['n_frames']
                self.frames[state] = slice_sprites(temp_image, n_frames, 
                                                   colorkey)
                self.fps[state] = self.source['states'][state]['fps']
            except KeyError:
                warn("***WARNING*** ", self.name, "is requesting sprite-info \
                      for state", state, "but none has been defined in the \
                      source. Using the first state as fall back")

                try: 
                    self.frames[state] = self.frames[self.frames.keys()[0]]
                    self.n_frames[state] = self.n_frames[self.frames.keys()[0]]
                    self.fps[state] = self.fps[self.frames.keys()[0]]
                except IndexError:
                    warn("***WARNING*** ", self.name, "has no state to fall \
                          back on yet, you will be able to continue playing, \
                          but if the game crashes it will likely be because \
                          it tried to enter this state.")
        
        if start_state is not None:
            if start_state in self.states:
                self.state = start_state
            else:
                warn("***WARNING*** ", start_state, "is not a defined state, \
                     using first state as backup.")
                self.state = self.states[0]
        else:
            self.state = self.states[0]

        self.delay = 1000/self.fps[self.state]

        self.start = pygame.time.get_ticks()
        self.last_update= 0
        self.frame = 0
        
        self.animate()

    def animate(self):
        """ Animeate based on clock ticks since last update. """
        t = pygame.time.get_ticks()

        if t - self.last_update > self.delay:
            self.frame += 1
            self.frame %= self.n_frames[self.state]
            self.last_update = t
            self.image = self.frames[self.state][self.frame]
        

