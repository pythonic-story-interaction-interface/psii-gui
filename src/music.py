import pygame, sys, os, random, warnings
from pygame.locals import *

import event as event_module
import data_representation as dr 
import toolbox as tb

class Zepp_Mixer(event_module.Event_Target):
    def __init__(self, owner):
        event_module.Event_Target.__init__(self, owner)

        self.register_listener(event_module.EVENT_SOUND_MASTER_TOGGLE, add_translation=event_module.EVENT_PRESS_MUTE)

        self.music_data = dr.Data_Representation_Zepp(of_what='music')      

        if not pygame.mixer:
            warnings.warn("Warning: Without sound there is no life! But you" + \
                          " are allowed to continue your miserable existance...")
            self.no_mixer = True
        else:
            self.no_mixer = False

        self.is_playing = False 
        self.next_song_p = 1
        self.music_volume = 0.5
        self.current_volume = 0
        self.target_volume = 0.5
        self.volume_steps = self.music_volume / 10

        if not self.no_mixer:
            try:
                pygame.mixer.music.set_volume(self.current_volume)
            except pygame.error:
                #HACK03 WTF..? Can we debug this?
                warnings.warn("Warning: Skyman's Error. Without sound there" + \
                               " no life! But you are allowed to continue your miserable existance...")
                self.no_mixer = True

        self.music_paths = {}
        self.times_of_day = ["level_dawn", "level_day", "level_dusk", "level_dark"]
        self.reverse_dict = {}
        self.day_sections = len(self.times_of_day)

        for i, item in enumerate(self.times_of_day):
            self.reverse_dict[item] = i

        self.time_of_day = self.times_of_day[0]

        self.version_index = 0

        for section in self.music_data.representation.sections():
            for option in self.music_data.representation.options(section):
                try:
                    self.music_paths[section].append(os.sep.join(self.music_data.representation.get(section, option).split("|")))
                except KeyError:
                    self.music_paths[section] = [os.sep.join(self.music_data.representation.get(section, option).split("|"))]

    def play_music(self, part_of_day=None):
        if not part_of_day:
            self.time_of_day = self.times_of_day[(self.reverse_dict[self.time_of_day]+1)%self.day_sections]
        else:
            if part_of_day in self.times_of_day:
                self.time_of_day = part_of_day

        #pseudoHACK02 on request, not releasing the other versions until game is more done
        version = 0 #random.randint(0, len(self.music_paths[self.time_of_day])-1)

        if self.no_mixer:
            print "Now it would be playing", self.time_of_day
        else:
            pygame.mixer.music.load(self.music_paths[self.time_of_day][version])
            pygame.mixer.music.play(0, 0.0)
            #pygame.mixer.music.set_endevent(event_module.EVENT_SOUND_MUSIC_ENDED)
        self.is_playing = True

    def fade(self):
        self.current_volume += self.volume_steps*tb.sign(self.target_volume-self.current_volume)
        if self.current_volume < 0:
            self.current_volume = 0
        elif self.current_volume > self.target_volume:
            self.current_volume = self.target_volume
        
        if self.no_mixer:
            print "Now the music would be fading in or out"
        else:
            pygame.mixer.music.set_volume(self.current_volume)
        
    def update(self):
        for event in self.event_queue:
            if event.event == event_module.EVENT_SOUND_MASTER_TOGGLE:
                if self.no_mixer:
                    print "Music would be toggled"
                else:
                    if pygame.mixer.music.get_volume() == 0:
                        self.event_handler.send(event_module.Event_Object(self, event_module.EVENT_LEVEL_MESSAGE, "UNMUTE"))
                        self.target_volume = self.music_volume
                    else:
                        self.event_handler.send(event_module.Event_Object(self, event_module.EVENT_LEVEL_MESSAGE, "MUTE"))
                        self.target_volume = 0
            else:
                print "*Mixer missed " + str(event.event)

        self.event_queue = []

        #if pygame.mixer.music.get_endevent == event_module.EVENT_SOUND_MUSIC_ENDED:
        #   self.is_playing = False

        if not self.no_mixer:
            if not pygame.mixer.music.get_busy():
                if random.uniform(0,1) < self.next_song_p:
                    self.play_music()

            if self.current_volume != self.target_volume:
                self.fade() 
