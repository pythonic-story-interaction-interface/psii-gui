#! /usr/bin/env python

import numpy as np
from scipy.misc import imread, imsave
from glob import glob

images = []
for f in glob('*_seed.png'):
    images.append(imread(f))
    print f + ": " + `images[-1].shape`
    

I = np.zeros(images[0].shape)

for im in images:
    I += im

I /= len(images)

imsave('new.png', I)
    
